package com.tobiaswillhorn.accessfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration.class})
public class AccessfilterApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(AccessfilterApplication.class, args);
    }

}
