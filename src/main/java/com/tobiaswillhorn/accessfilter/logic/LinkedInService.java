package com.tobiaswillhorn.accessfilter.logic;

import com.google.gson.Gson;
import com.tobiaswillhorn.accessfilter.object.LinkedInAccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

@Component
@PropertySource("classpath:social-cfg.properties")
public class LinkedInService {
    @Value("${linkedin.consumer.key}")
    private String client_id;
    @Value("${linkedin.consumer.secret}")
    private String app_secret;
    @Value("${linkedin.redirect_url}")
    private String redirect_url;
    @Value("${linkedin.scope}")
    private String scope;
    @Value("${linkedin.state}")
    private String state;

    public String getClient_id() {
        return client_id;
    }

    public String getApp_secret() {
        return app_secret;
    }

    public String getRedirect_url() {
        return redirect_url;
    }

    public String getScope() {
        return scope;
    }

    public String getState() {
        return state;
    }

    public LinkedInAccessToken getAccessToken(String code) throws IOException {
        URL url = new URL("https://www.linkedin.com/oauth/v2/accessToken");
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host","www.linkedin.com");
        connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        String param = "grant_type=authorization_code&code=" + code + "&redirect_uri=" + this.redirect_url +
                "&client_id=" + this.client_id + "&client_secret=" + this.app_secret;
        connection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(param);
        wr.flush();
        wr.close();

        int responseCode = connection.getResponseCode();

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        StringBuffer buffer = new StringBuffer();
        while ((line = reader.readLine()) != null){
            buffer.append(line);
        }
        reader.close();
        String accessTokenJSON = buffer.toString();
        LinkedInAccessToken accessToken = new Gson().fromJson(accessTokenJSON, LinkedInAccessToken.class);
        return accessToken;
    }

    public String getConnections(LinkedInAccessToken accessToken) throws IOException {
        URL url = new URL("https://api.linkedin.com/v2/connections?q=viewer");
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Host","api.linkedin.com");
        connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        String param = "";
        connection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(param);
        wr.flush();
        wr.close();

        int responseCode = connection.getResponseCode();

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        StringBuffer buffer = new StringBuffer();
        while ((line = reader.readLine()) != null){
            buffer.append(line);
        }
        reader.close();
        String accessTokenJSON = buffer.toString();
        return "";
    }
}
