package com.tobiaswillhorn.accessfilter.object;

public class LinkedInAccessToken {
    private String access_token;
    private double expires_in;

    public LinkedInAccessToken(String access_token, double expires_in) {
        this.access_token = access_token;
        this.expires_in = expires_in;
    }

    public LinkedInAccessToken() {
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public double getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(double expires_in) {
        this.expires_in = expires_in;
    }
}
