$(document).ready(function () {
    $('#login-fb').on('click', function () {
        FB.login(function (response) {
            if (response.authResponse) {
                console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function (response) {
                    alert('Good to see you, ' + response.name + '.');
                });
            }
        }, {
            scope: 'public_profile,email,user_likes'
        });
    });
});