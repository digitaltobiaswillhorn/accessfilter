<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/resources/dist/semantic-ui/semantic.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300" rel="stylesheet">
    <link rel="stylesheet" href="/resources/css/style.css">
</head>
<body>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '399252764251709',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v3.3'
        });
    };
</script>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>

<div class="main">
    <div class="container-fluid h-100">
        <div class="row justify-content-end align-items-center h-100">
            <div class="col-sm-12 col-md-7 h-75 text-center text-white" >
                <h1 class="filter-title" style="font-family: 'Montserrat', sans-serif;">L'application de gestion tout-en-un</h1>
            </div>
            <div class="col-sm-12 col-md-5 form-container h-100">
                <div class="container h-100 align-items-center">
                    <div class="row justify-content-center align-items-center h-100">
                        <div class="col text-center">
                            <div class="card carding">
                                <div class="row">
                                    <div class="col-12 text-center mb-4">
                                        <img src="/resources/img/logo.png" alt="Logo de l'application Slotty">
                                    </div>
                                    <div class="col-12 text-center mb-4">
                                        <button class="ui facebook button" id="login-fb">
                                            <i class="facebook icon"></i>
                                            Se connecter via Facebook
                                        </button>
                                    </div>
                                    <div class="col-12 text-center mb-4">
                                        <a class="ui linkedin button"
                                           href="https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${ property.client_id }&redirect_uri=${ property.redirect_url }&state=${ property.state }&scope=${ property.scope }">
                                            <i class="linkedin icon"></i>
                                            Se connecter via Linkedin
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%--Javascript Source--%>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="/resources/dist/semantic-ui/semantic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/ionicons@4.5.5/dist/ionicons.js"></script>
<script src="/resources/js/module.js"></script>

</body>
</html>